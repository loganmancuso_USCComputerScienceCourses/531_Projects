/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_CORE2_TAB_H_INCLUDED
# define YY_YY_CORE2_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    PROGRAM = 258,
    BEGINT = 259,
    INPUTT = 260,
    OUTPUTT = 261,
    ENDT = 262,
    IF = 263,
    THEN = 264,
    ENDIF = 265,
    ELSE = 266,
    WHILE = 267,
    LOOP = 268,
    ENDLOOP = 269,
    DOUBLE = 270,
    FLOAT = 271,
    ID = 272,
    INT = 273,
    REAL = 274,
    RELOP = 275,
    INTTYPE = 276,
    DOUBLETYPE = 277,
    FLOATTYPE = 278,
    REALTYPE = 279,
    AND = 280,
    NOT = 281,
    OR = 282,
    LPAREN = 283,
    RPAREN = 284,
    ASSOP = 285,
    BAR = 286,
    TIMES = 287,
    DIV = 288,
    PLUS = 289,
    MINUS = 290,
    MOD = 291,
    LT = 292,
    GT = 293,
    EQT = 294,
    SEMICOLON = 295,
    COLON = 296,
    DOT = 297,
    COMA = 298,
    DBQUOTE = 299,
    SQUOTE = 300,
    NEWLINE = 301,
    LIST = 302
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 40 "core2.y" /* yacc.c:1909  */

	int type;
	int ival;
	char real;
	struct nlist *place;
	struct {
		LIST  *true;
		LIST  *false;
	} list;
	int quad;
	LIST  next;

#line 115 "core2.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_CORE2_TAB_H_INCLUDED  */
