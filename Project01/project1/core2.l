%{
/*****************************************************************
 * 'core2.l'
 * core2.l is a flex specification file
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 03-27-2018--06:49:16
 * THIS IS A TEST FILE CHANGES WILL NOT BE SAVED
**/

#include "core2.tab.h"
%}

digit [0-9]
letter [a-zA-Z]

%%

\n	printf("'new line' Token Found\n"); return('\n');
program	printf("'program' Token Found\n"); return(PROGRAM);
begin	printf("'begin' Token Found\n"); return(BEGINT); /*core specific notation*/
input	printf("'input' Token Found\n"); return(INPUTT);
output	printf("'output' Token Found\n"); return(OUTPUTT);
end	printf("'end' Token Found\n"); return(ENDT);

int	printf("'int type' Token Found\n"); return (INTTYPE);

\;	return(SEMICOLON);
\:	return(COLON);
\,	return(COMMA);
\:\=	printf("'assignment op' Token Found\n"); return(ASSOP);
\+	return(PLUS);
\*	return(TIMES);
\-	return(MINUS);
\/	return(DIV);
\(	return(LPAREN);
\)	return(RPAREN);

{digit}+	return(UINT);
{letter}+	printf("'identifier' Token Found\n"); return(ID);


%%

yywrap() {

}/*end yywrap*/

yyerror (char const *s) {
  fprintf (stderr, "%s\n", s);
} /*end yyerror */


/****************************************************************
 * End 'core2.l'
**/