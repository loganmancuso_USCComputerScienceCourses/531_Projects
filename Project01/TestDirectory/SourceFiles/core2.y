
%{
/*****************************************************************
 * 'core2.y'
 * core2.y is a grammer specification file
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 03-27-2018--06:49:16
 * THIS IS A TEST FILE CHANGES WILL NOT BE SAVED
**/
yydebug=1;

%}

%token PLUS MINUS NEGATIVE TIMES DIV LPAREN RPAREN COMMA SEMICOLON COLON/*general*/
%token UINT /*values*/
%token INTTYPE /*types*/
%token BEGINT ENDT PROGRAM ASSOP ID INPUTT OUTPUTT /*core specific*/

%%
/*program line in core*/
line	:	program '\n'	{
								printf("recognized a program \n"); 
								exit(0);
							}
	;
program	:	PROGRAM '\n' declaration BEGINT '\n'statement ENDT SEMICOLON
	;
statement	:	body '\n' statement
	|	body '\n'
	;
/*core specific assignments*/
body	:	input
	|	output
	|	assignment
	;
input	:	INPUTT idlist SEMICOLON
	;
output	:	OUTPUTT idlist SEMICOLON
	;
declaration	: type COLON idlist SEMICOLON '\n' declaration
	|	type COLON idlist SEMICOLON '\n'
	;
idlist	:	ID COMMA idlist
	|	ID
	;
assignment	:	ID ASSOP expr
	;
expr	:	expr PLUS sub
	|	sub
	;
sub : sub MINUS term
	|	term
	;
term	:	term TIMES divisor
	|	divisor
	;
divisor : divisor DIV factor
	|	factor
	;
factor	:	LPAREN  expr  RPAREN	
	|	UINT
	|	ID
	;
type	:	INTTYPE
	;
%%
/****************************************************************
 * End 'core2.y'
**/
