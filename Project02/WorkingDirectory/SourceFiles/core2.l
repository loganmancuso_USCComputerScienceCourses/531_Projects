%{
/*****************************************************************
 * 'core2.l'
 * core2.l is a flex specification file
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 03-30-2018--17:44:52
**/

#include "core2.tab.h"
#include "kr.symtab.h"

struct nlist *place;

%}

digit [0-9]
letter [a-zA-Z]

%%

program	printf("'program' Token Found '%s'\n", yytext);	return(PROGRAM); /*core specific notation*/
begin	printf("'begin' Token Found '%s'\n", yytext);	return(BEGINT); 
input	printf("'input' Token Found '%s'\n", yytext);	return(INPUTT);
output	printf("'output' Token Found '%s'\n", yytext);	return(OUTPUTT);
end	printf("'end' Token Found '%s'\n", yytext);	return(ENDT);

if	printf("'if' Token Found '%s'\n", yytext);	return(IF);
then	printf("'then' Token Found '%s'\n", yytext);	return(THEN);
"end if"	printf("'end if' Token Found");	return(ENDIF);
else	printf("'else' Token Found '%s'\n", yytext);	return(ELSE);
while	printf("'while' Token Found\n");	return(WHILE);
loop	printf("'loop' Token Found");	return(LOOP);
"end loop"	printf("'end loop' Token Found");	return(ENDLOOP);

int	printf("'int type' Token Found '%s'\n", yytext);	return	(INTTYPE);
double	printf("'double type' Token Found '%s'\n", yytext);	return(DOUBLETYPE);
float	printf("'float type' Token Found '%s'\n", yytext);	return(FLOATTYPE);
real	printf("'real type' Token Found '%s'\n", yytext);	return(REALTYPE);

[+-]?(({digit}+)|({digit}*'.'{digit}+)([eE][+-]?{digit}+)?) {
		printf("'real number' Token Found  '%s'\n", yytext);
		yylval.real = atoi(yytext);
		return(REAL);
	} /*{real}	return(REAL);*/
[+-]{digit}+.{digit}+	return(FLOAT);
[+-]{digit}+.{digit}{digit}	return(DOUBLE);
[+-]{digit}+	return(INT);

{letter}({letter}|{digit})* {
		printf("'identifier' Token Found '%s' \n", yytext);
		yylval.place = install(strdup(yytext));
		return(ID);

	/*		printf ("\nUndeclared Variable: '%s' \nError at line: %d \n",yytext,yylineno); */
	}

\(	return(LPAREN);
\)	return(RPAREN);
\:\=	printf("'assignment op' Token Found '%s'\n", yytext);	return(ASSOP);
\<	yylval.relop=1;	return(RELOP);
\=	yylval.relop=2;	return(RELOP);
\!\=	yylval.relop=3;	return(RELOP);
\>	yylval.relop=4;	return(RELOP);
\|	return(BAR);

\+	return(PLUS);
\-	return(MINUS);
\*	return(TIMES);
\/	return(DIV);
\%	return(MOD);

\;	return(SEMICOLON);
\:	return(COLON);
\.	return(DOT);
\,	return(COMA);
\"	return(DBQUOTE);
\'	return(SQUOTE);

\n	printf("'new line' Token Found \n");	return(NEWLINE);
[ \t]	;
.	printf ("\nUndefined Character: ''%s'' \nError at line: %d\n",yytext,yylineno);

%%

yywrap() {

}/*end yywrap*/

void
mywhere() {
	fprintf(stderr, "line %d", yylineno);
}/*mywhere*/

void yyerror(char *s, char *t) {
	extern int yynerrs;
	static int list = 0;
	if(s || ! list) {
		fprintf(stderr, "[error %d] ", yynerrs+1);
		mywhere();
		if(s){
			fputs(s, stderr);
			putc('\n', stderr);
			return;
		}
		if(t){
			fputs("expecting: ", stderr);
			fputs(t, stderr);
			list = 1;
			return;
		}
		fputs("syntax error\n",stderr);
		return;
	}
	if(t) {
		putc(' ', stderr);
		fputs(t, stderr);
		return;
	}
	putc('\n', stderr);
	list = 0;
}/*end yyerror*/



/****************************************************************
 * End 'core2.l'
**/